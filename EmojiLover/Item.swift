//
//  Item.swift
//  EmojiLover
//
//  Created by Vladislava Bogaturova on 23.10.2023.
//

import Foundation
import SwiftData

@Model
final class Item {
    var timestamp: Date
    
    init(timestamp: Date) {
        self.timestamp = timestamp
    }
}
